﻿/// <reference path="../App.js" />

(function () {
    "use strict";
    var postid = "";
    var bookingid = 0;
    var thisitem;
    // The Office initialize function must be run each time a new page is loaded
    Office.initialize = function (reason) {
        $(document).ready(function () {
            app.initialize();
            thisitem = Office.context.mailbox;
            displayItemDetails();
            thisitem.item.loadCustomPropertiesAsync(GETcustomPropsCallback);
        });
    };

    // Displays the "Subject" and "FromDate", ToDate,RoomLocation fields, based on the current item
    function displayItemDetails() {
        getSubject();
        getLocalStartTime();
        getLocalEndTime();
        getLocation();
        if (window.location.search.indexOf("postid") > -1) {
            postid = window.location.search;
        }
        $("#editbooking").attr("href", window.location.href.replace("AppRead", "AppCompose")+"&edit=true");
    }
    //Subject
    function getSubject() {
        thisitem.item.subject.getAsync(
   function (asyncResult) {
       if (asyncResult.status == Office.AsyncResultStatus.Failed) {
           meetingsubject = "";
       }
       else {
           $('#subject').text(asyncResult.value == "" ? "Untitled Booking" : asyncResult.value);
       }
   });
    }
    function getLocalStartTime() {
        thisitem.item.start.getAsync(
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    localstarttime = ""
                }
                else {
                    $('#fromdate').text(asyncResult.value.toDateString() + " " + asyncResult.value.toLocaleTimeString());
                }
            });

    }
    function getLocalEndTime() {
        thisitem.item.end.getAsync(
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    localendtime = "";
                }
                else {
                    $('#todate').text(asyncResult.value.toDateString() + " " + asyncResult.value.toLocaleTimeString());
                }
            });
    }
    function getLocation()
    {
        thisitem.item.location.getAsync(
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    localendtime = "";
                }
                else {
                    $('#location').text(asyncResult.value.replace("<<", "").replace(">>", ""));
                }
            });
    }
    function GETcustomPropsCallback(asyncResult)
    {
        var customProps = asyncResult.value;
        var condecoPropPost = customProps.get("bookingid");
        bookingid = condecoPropPost;
        $('#bookingid').text(bookingid);

    }
})();