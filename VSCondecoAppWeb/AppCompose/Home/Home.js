﻿/// <reference path="C:\RNMDEVELOPMENT\VSCondecoApp\VSCondecoAppWeb\AppRead/Home/Home.html" />
/// <reference path="C:\RNMDEVELOPMENT\VSCondecoApp\VSCondecoAppWeb\AppRead/Home/Home.html" />
/// <reference path="../App.js" />

(function () {
    'use strict';
    var messageBanner;
    var ssoref;
    var UserItems;
    var HostDetails;
    var JsonuserDefaultSetting;
    var Jsoncountriesinfo;
    var starttime;
    var endtime;
    var localstarttime;
    var localendtime;
    var html = "";
    var sessiontoken = "";
    var meetingsubject = "";
    var postid = "";
    var thisitem;
    var edit = false;
    var bookingid = 0;
    // The initialize function must be run each time a new page is loaded
    Office.initialize = function (reason) {
        $(document).ready(function () {
            app.initialize();
            //getconnectionstatus();
            thisitem = Office.context.mailbox;
            thisitem.item.loadCustomPropertiesAsync(GETcustomPropsCallback);
            //$('#set-subject').click(setSubject);
            //$('#get-subject').click(getSubject);
            //$('#add-to-recipients').click(addToRecipients);
            $('#btnFind').click(RoomsInfo);
            // Initialize the FabricUI notification mechanism and hide it
            var element = document.querySelector('.ms-MessageBanner');
            messageBanner = new fabric.MessageBanner(element);
            messageBanner.hideBanner();
            
            if (localStorage.getItem("CondecoToken") == null || localStorage.getItem("CondecoToken") == "") {
                openDialog();
            }
            else {
                sessiontoken = localStorage.getItem('CondecoToken');
                UserInfo(sessiontoken);
            }
            $("#maintable").on("click", 'button', function () {
                AddRoomBooking(this);
            });

            $("body").on("mouseenter", "div", function ()
            {
                getStartTime();
                getEndTime();
                getSubject();
                getLocalStartTime();
                getLocalEndTime();
            });
        });
        if (window.location.search.indexOf("postid") > -1) {
            postid = window.location.search;
        }
        if (window.location.search.indexOf("edit") > -1) {
            edit = true;
        }
        if (postid != undefined && postid > 0) {

        }
        
    };
    function addToRecipients() {
        var item = thisitem.item;
        var addressToAdd = {
            displayName: thisitem.userProfile.displayName,
            emailAddress: thisitem.userProfile.emailAddress
        };

        if (item.itemType === Office.MailboxEnums.ItemType.Message) {
            Office.cast.item.toMessageCompose(item).to.addAsync([addressToAdd]);
        } else if (item.itemType === Office.MailboxEnums.ItemType.Appointment) {
            Office.cast.item.toAppointmentCompose(item).requiredAttendees.addAsync([addressToAdd]);
        }
    }
    function getconnectionstatus() {
        $.ajax({
            type: 'GET',
            url: location.protocol + "//" + location.host + '/webapi/OfficeApp/getconnectionstatus',
            cache: false,
            timeout: 10000
        })
            .success(function (data) {
                if (data != null && data) {
                    var response = JSON.parse(data);
                    if (response.success == "True" && response.messgage == "Ok") {
                    }
                }
            })
        .fail(function (status) {
           showNotification("Condeco Application status is " + status);

        });
    }
    // Opens the dialog.
    function openDialog() {
        // Reference the Form.html file.
        var dialogUrl = location.protocol + "//" + location.host + location.pathname.replace("AppCompose/home/home.html", "LoginForm.html?appmethod=open");
        // Display the dialog.
        Office.context.ui.displayDialogAsync(dialogUrl, { width: 35, height: 45, requireHTTPS: true, displayInIframe: false }, function (asyncResult) {
            if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
                // TODO: Handle error.
                return;
            }
            // Get the dialog and register event handlers.
            var dialog = asyncResult.value;
            dialog.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogMessageReceived, function (asyncResult) {
                if (asyncResult.type !== Microsoft.Office.WebExtension.EventType.DialogMessageReceived) {
                    // TODO: Handle unknown message.
                    return;
                }
                var data = JSON.parse(asyncResult.message);
                if (data != undefined) {
                    localStorage.setItem('CondecoToken', data.TOKEN);
                    //localStorage.setItem('CondecoURL', data.CondecoURL);
                    sessiontoken = localStorage.getItem('CondecoToken');
                    UserInfo(sessiontoken);
                }
                dialog.close();
                thisitem.item.loadCustomPropertiesAsync(GETcustomPropsCallback);

            });
        });
        //  }
    }
    function showNotification(header, content) {
        $("#notificationHeader").text(header);
        $("#notificationBody").text(content);
        messageBanner.showBanner();
        messageBanner.toggleExpansion();
        setInterval(function () { $("#ms-MessageBannerX").click(); }, 4000);
    }
    function UserInfo(token) {
        $.ajax({
            url: location.protocol + "//" + location.host + '/webapi/OfficeApp/UserInfo',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            data: {},
            success: function (response) {
                var apiresonse = JSON.parse(response);
                JsonuserDefaultSetting = apiresonse.DefaultSetting;
                Jsoncountriesinfo = apiresonse.Countries;
                Jsoncountriesinfo.sort();
                generatecounty(Jsoncountriesinfo);
            },
            error: function (error) { },
        });
    }
    function RoomsInfo()
    {
        if ($("#ddlCounties")[0].value == -2) {
            showNotification("","Please select a country");
        }
        else if ($("#ddlLocations")[0].value == -2)
        {
            showNotification("","Please select a location");
        }
        else if ($("#ddlFloors")[0].value == -2) {
            showNotification("","Please select a floor");
        }
        else {
            var floors = getfloors();
            var roomSearchRequest = { "token": localStorage.getItem('CondecoToken'), "startDate": starttime.substring(0, starttime.lastIndexOf(".")) + "Z", "endDate": endtime.substring(0, endtime.lastIndexOf(".")) + "Z", "groupIds": 0, "locationIds": $("#ddlLocations")[0].value, "floorNums": floors, "languageId": "1", "pageIndex": "0", "pageSize": "0", "numberAttending": "1", "roomAttributes": [] };
            $.ajax({
                url: location.protocol + "//" + location.host + '/webapi/OfficeApp/RoomsInfo',
                type: 'GET',
                cache: false,
                data: roomSearchRequest,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('CondecoToken')
                },
            }).success(function (response) {
                if (response != null && response) {
                    var apiresonse = JSON.parse(response);
                    var success = apiresonse.Success;
                    if (success == false)
                    {
                        showNotification("", apiresonse.Error.ErrorDescription);
                    }
                    var totalrecords = parseInt(apiresonse.TotalRecords);
                    if (totalrecords > 0) {
                        var GetRoomsInfo = apiresonse.RoomSearchResult;
                        GenerateRoomData(GetRoomsInfo);
                    }
                    else {
                    }
                }
            }).fail(function (status) {

            }).always(function () {

            });
        }
    }
    function GetPostID(Room) {
        var iRecDates = "";
        var iAttendee = "";
        var iEmail = "";
        var thisDomain = "";
        var ssoUser = "";
        var iMeetingType = "";
        var iSwitch = 1;
        var currentTZ = "";
        var PostRequest = { "Title": meetingsubject, "StartDate": formatlocaldate(localstarttime), "EndDate": formatlocaldate(localendtime), "Email": iEmail, "Attendees": iAttendee, "RecDates": iRecDates, "TimeZone": Office.context.mailbox.userProfile.timeZone, "MeetingType": iMeetingType, "OutlookPostId": postid };
        $.ajax({
            url: location.protocol + "//" + location.host + '/webapi/OfficeApp/SaveOutlookPostData',
            type: 'POST',
            data: PostRequest,
            dataType: 'json',
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('CondecoToken')
            },
        }).success(function (response) {
            if (response != null && response) {
                var apiresonse = JSON.parse(response);
                var success = apiresonse.Success;
                if (apiresonse.Success == true) {
                    postid = apiresonse.PosID;
                }
            }
        }).fail(function (status) {

        }).always(function () {

        });
    }
    function UpdatePostID(outlookpostID, bookingID) {
        var dataObject = JSON.stringify({ 'outlookpostID': outlookpostID, 'bookingID': bookingID });
        $.ajax({
            url: location.protocol + "//" + location.host + '/webapi/OfficeApp/UpdateOutlookPostData?outlookpostID=' + outlookpostID + "&bookingID=" + bookingID,
            type: 'POST',
            //data: dataObject,
            //dataType: "json",
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('CondecoToken')
            },
        }).success(function (response) {
            if (response != null && response) {
                var apiresonse = JSON.parse(response);
                var success = apiresonse.Success;
            }
        }).fail(function (status) {

        }).always(function () {

        });
    }
    function AddRoomBooking(Room) {
        // var floors = getfloors();
        GetPostID(Room);
        //sendRequest();
        var Roomtitlevalues = Room.title.split(",");
        var roomid = Room.value;

        var floorid = Roomtitlevalues[0].substring(Roomtitlevalues[0].lastIndexOf("=") + 1, Roomtitlevalues[0].length);
        var groupid = Roomtitlevalues[1].substring(Roomtitlevalues[1].lastIndexOf("=") + 1, Roomtitlevalues[1].length);
        var locationid = Roomtitlevalues[2].substring(Roomtitlevalues[2].lastIndexOf("=") + 1, Roomtitlevalues[2].length);
        var roomname = Roomtitlevalues[3].substring(Roomtitlevalues[3].lastIndexOf("=") + 1, Roomtitlevalues[3].length);
        var countryID = $("#ddlCounties")[0].value;
        var countryname = $("#ddlCounties option:selected").text();
        var floorname = $("#ddlFloors option:selected").text();
        var locationname = $("#ddlLocations option:selected").text();

        var roomBookRequest = { "FloorName": floorname, "FloorNum": parseInt(floorid), "NumAttending": 1, "CountryID": parseInt(countryID), "CountryName": countryname, "MeetingTitle": meetingsubject, "LocationID": parseInt(locationid), "LocationName": locationname, "TimeTo": endtime.substring(0, endtime.lastIndexOf(".")) + "Z", "TimeFrom": starttime.substring(0, starttime.lastIndexOf(".")) + "Z", "RoomID": parseInt(roomid), "RoomName": roomname, "BookingID": 0, "LanguageID": 1, "TimeZone": Office.context.mailbox.userProfile.timeZone };
        $.ajax({
            url: location.protocol + "//" + location.host + '/webapi/OfficeApp/AddRoomBooking',
            type: 'POST',
            data: roomBookRequest,
            dataType: 'json',
            headers: {
                "Authorization": "Bearer " + localStorage.getItem('CondecoToken')
            },
        }).success(function (response) {
            if (response != null && response) {
                var apiresonse = JSON.parse(response);
                var success = apiresonse.Success;
                if (apiresonse.Success == true) {
                    setLocation("");
                    setLocation("<< Room " + apiresonse.RoomBooking.BookingData.RoomName + " " + ": Floor: " + apiresonse.RoomBooking.GeneralData.FloorNum + " >>");
                    //Office.cast.item.toItemCompose(thisitem.item).location.setAsync("<< Room " + apiresonse.RoomBooking.BookingData.RoomName + " " + ">>" + "<< Floor: " + apiresonse.RoomBooking.GeneralData.FloorNum + " >>");
                    showNotification("", "Booking ID: " + apiresonse.RoomBooking.BookingData.BookingID + " created for room: " + apiresonse.RoomBooking.BookingData.RoomName);
                    setSubject(meetingsubject);
                    UpdatePostID(postid, apiresonse.RoomBooking.BookingData.BookingID);
                    bookingid = apiresonse.RoomBooking.BookingData.BookingID;
                    thisitem.item.loadCustomPropertiesAsync(SETcustomPropsCallback);
                    RoomsInfo();
                    thisitem.item.loadCustomPropertiesAsync(GETcustomPropsCallback);
                }

            }
        }).fail(function (status) {

        }).always(function () {

        });


    }
    function generatecounty(jsonData) {
        //$(".containersearch").css("visibility", "visible");
        $("#containersearch").show();
        $("#ddlCounties").empty();
        $("#ddlLocations").empty();
        $("#ddlFloors").empty();
        var listItemscountry = '<option selected="selected" value="-2">- Select -</option>';
        var listItemsLocations = '<option selected="selected" value="-2">- Select -</option>';
        var listItemsFloors = '<option selected="selected" value="-2">- Select -</option>';
        $("#ddlCounties").width(150);
        $("#ddlLocations").width(150);
        $("#ddlFloors").width(75);
        for (var i = 0; i < jsonData.length; i++) {
            listItemscountry += "<option value='" + jsonData[i].Id + "'>" + jsonData[i].Name + "</option>";
        }
        $("#ddlCounties").html(listItemscountry);
        $("#ddlLocations").html(listItemsLocations);
        $("#ddlFloors").html(listItemsFloors);
        $("#ddlCounties").on("change", function (event) {
            generatelocation(this);
        });
        $("#ddlLocations").on("change", function (event) {
            generatefloor(this);
        });
        setuserdefaults(JsonuserDefaultSetting, "country");
        setuserdefaults(JsonuserDefaultSetting, "location");
        setuserdefaults(JsonuserDefaultSetting, "floor");

    }
    function generatelocation(countryid) {
        $("#ddlLocations").empty();
        $("#ddlFloors").empty();
        var countrylocations = jQuery.grep(Jsoncountriesinfo, function (a) { return a.Id == parseInt(countryid.value); })
        var listItemsLocations = '<option selected="selected" value="-2">- Select -</option>';
        var listItemsFloors = '<option selected="selected" value="-2">- Select -</option>';
        for (var i = 0; i < countrylocations[0].Locations.length; i++) {
            listItemsLocations += "<option value='" + parseInt(countrylocations[0].Locations[i].Id) + "'>" + countrylocations[0].Locations[i].Name + "</option>";
        }
        $("#ddlLocations").html(listItemsLocations);
        $("#ddlFloors").html(listItemsFloors);
        $("#ddlLocations").width(165);



    }
    function generatefloor(locationId) {
        $("#ddlFloors").empty();

        var listItemsFloors = '<option selected="selected" value="-2">- Select -</option>';
        var countryval = $("#ddlCounties")[0].value;
        var locationval = $("#ddlLocations")[0].value;
        var userselectedlocation = jQuery.grep(Jsoncountriesinfo, function (a) { return a.Id == parseInt(countryval); })
        var floors = jQuery.grep(userselectedlocation[0].Locations, function (a) { return a.Id == parseInt(locationval); })
        if (floors[0].Floors.length > 0) {
            listItemsFloors += '<option selected="-All Floors-" value="-1">- All Floors -</option>';
        }
        for (var i = 0; i < floors[0].Floors.length; i++) {
            listItemsFloors += "<option value='" + parseInt(floors[0].Floors[i].Floornum) + "'>" + floors[0].Floors[i].Name + "</option>";
        }
        $("#ddlFloors").html(listItemsFloors);
        $("#ddlFloors").width(75);

    }
    function setuserdefaults(userdefaults, item) {
        if (userdefaults != undefined) {
            switch (item) {
                case "country":
                    if (userdefaults.Country != undefined && userdefaults.Country != "") {
                        $("#ddlCounties").val(userdefaults.Country != undefined ? userdefaults.Country : -2);
                    }
                    break;
                case "location":
                    if (userdefaults.Location != undefined && userdefaults.Location != "") {
                        $("#ddlLocations").empty();
                        var defaultcountry = jQuery.grep(Jsoncountriesinfo, function (a) { return a.Id == parseInt(userdefaults.Country); })
                        var listItemsLocations = '<option selected="selected" value="-2">- Select -</option>';
                        for (var i = 0; i < defaultcountry[0].Locations.length; i++) {
                            listItemsLocations += "<option value='" + parseInt(defaultcountry[0].Locations[i].Id) + "'>" + defaultcountry[0].Locations[i].Name + "</option>";
                        }
                        $("#ddlLocations").html(listItemsLocations);
                        $("#ddlLocations").val(userdefaults.Location != undefined ? userdefaults.Location : -2);
                    }
                    break;
                case "floor":
                    if (userdefaults.Floor != undefined && userdefaults.Floor != "") {
                        $("#ddlFloors").empty();
                        var listItemsFloors = '<option selected="selected" value="-2">- Select -</option>';

                        var countryval = $("#ddlCounties")[0].value;
                        var locationval = $("#ddlLocations")[0].value;
                        var userselectedlocation = jQuery.grep(Jsoncountriesinfo, function (a) { return a.Id == parseInt(countryval); })
                        var floors = jQuery.grep(userselectedlocation[0].Locations, function (a) { return a.Id == parseInt(locationval); })
                        if (floors[0].Floors.length > 0) {
                            listItemsFloors += '<option selected="-All Floors-" value="-1">- All Floors -</option>';
                        }
                        for (var i = 0; i < floors[0].Floors.length; i++) {
                            listItemsFloors += "<option value='" + parseInt(floors[0].Floors[i].Floornum) + "'>" + floors[0].Floors[i].Name + "</option>";
                        }
                        $("#ddlFloors").html(listItemsFloors);
                        $("#ddlFloors").val(userdefaults.Floor != undefined ? parseInt(userdefaults.Floor) : -1);
                        $("#ddlFloors").width(75);
                    }
                    break;
                default:
                    break;
            }
        }
    }
    function getStartTime() {
        thisitem.item.start.getAsync
            (
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    starttime = "";
                }
                else {
                    starttime = asyncResult.value.toISOString();
                }
            });
    }
    function getEndTime() {
        thisitem.item.end.getAsync(
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    endtime = "";
                }
                else {
                    endtime = asyncResult.value.toISOString();
                }
            });
    }
    function getLocalStartTime() {
        thisitem.item.start.getAsync(
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    localstarttime = ""
                }
                else {
                    localstarttime = asyncResult.value;
                }
            });

    }
    function getLocalEndTime() {
        thisitem.item.end.getAsync(
            function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    localendtime = "";
                }
                else {
                    localendtime = asyncResult.value;
                }
            });
    }
    //Subject
    function getSubject() {
        thisitem.item.subject.getAsync(
   function (asyncResult) {
       if (asyncResult.status == Office.AsyncResultStatus.Failed)
       {
           meetingsubject = "";
       }
       else {
           meetingsubject = asyncResult.value == "" ? "Untitled Booking" : asyncResult.value;
       }
   });
    }
    function formatlocaldate(dateobject) {
        var date = "";
        if (dateobject != undefined) {
            var d = new Date(dateobject);
            var weekday = new Array(7); weekday[0] = "Sun"; weekday[1] = "Mon"; weekday[2] = "Tue"; weekday[3] = "Wed"; weekday[4] = "Thu"; weekday[5] = "Fri"; weekday[6] = "Sat";
            var weekD = weekday[d.getDay()];
            var day = d.getDate(); var month = d.getMonth() + 1; var year = d.getFullYear(); var hours = d.getHours(); var minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
            if (day < 10) { day = "0" + day; }
            if (month < 10) { month = "0" + month; }
            date = weekD + " " + day + "/" + month + "/" + year + " " + hours + ":" + minutes;
        }
        return date;
    }
    function GenerateRoomData(RoomsInfo) {
        $("#maintable").find("tr:gt(0)").remove();
        if (RoomsInfo != undefined && RoomsInfo.length > 0) {
            for (var i = 0; i < RoomsInfo.length; i++) {
                $("#maintable").append("<tr width=100%, border=1px>" + "<th width=40% align=left style=font-family:sans-serif;font-size:10px;font-weight:600>" + RoomsInfo[i].Name + "</th>" + "<th width=30% align=left style=font-family:sans-serif;font-size:10px;font-weight:600>" + "Floor: " + RoomsInfo[i].FloorID + "</th>" + "<th width=20% align=center style=font-family:sans-serif;font-size:10px;font-weight:600>" + RoomsInfo[i].MaxCapacity + "</th>" + "<th width=20%>" + "<button value=" + RoomsInfo[i].ResourceItemID + " title=FloorID=" + RoomsInfo[i].FloorID + ",GroupID=" + RoomsInfo[i].GroupID + ",LocationId=" + RoomsInfo[i].LocationId + ",RoomName=" + RoomsInfo[i].Name + " id=btnbookroom" + " type=button" + " class=btn btn-primary btn-sm" + ">" + "Book" + "</th>" + "</tr>")
            }
            //$('.divTable').append(html);
        }
    }
    function getfloors() {
        var floorval = new Array();
        if ($("#ddlFloors") != undefined)
        {
            if ($("#ddlFloors")[0].value == -2)
            {
                floorval.push(-2);
            }
            else if ($("#ddlFloors")[0].value == -1)
            {
               $('#ddlFloors option').each(function ()
               {
                   if ($(this).val() > -1) {
                       floorval.push(this.value);
                   }
               });
            }
            else
            {
                floorval = floorval.push(parseInt($('#ddlFloors').val()));
            }
        }
        return floorval;
    }
    function GETcustomPropsCallback(asyncResult) {
        var customProps = asyncResult.value;
        var condecoPropPost = customProps.get("postid");
        postid = condecoPropPost;
        editmode(postid);
    }
    function SETcustomPropsCallback(asyncResult) {
        var customProps = asyncResult.value;
        customProps.set("postid", postid);
        customProps.set("bookingid", bookingid);
        customProps.saveAsync(saveCallback);

    }
    function saveCallback(asyncResult) {
    }
    function setSubject(strtitle) {
        Office.cast.item.toItemCompose(thisitem.item).subject.setAsync(strtitle == "" ? "Untitled Booking" : strtitle);
    }
    function getSubject() {
        Office.cast.item.toItemCompose(thisitem.item).subject.getAsync(function (result) {
            return result.value;
        });
    }
    function setLocation(strlocation) {
        if (strlocation != undefined && strlocation != "") {
            Office.cast.item.toItemCompose(thisitem.item).location.setAsync(strlocation);
        }
    }
    function editmode(postid) {
        if (sessiontoken != undefined && postid != undefined) {
            if (postid != "" && sessiontoken != "" && edit==false) {
                var editurl = location.protocol + "//" + location.host + location.pathname.replace("AppCompose", "AppRead") + "?postid=" + postid;
                window.location.href = editurl;
            }
        }
    }
})();