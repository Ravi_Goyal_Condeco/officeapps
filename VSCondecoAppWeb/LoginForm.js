﻿(function () {
    "use strict";
    var redirecturi;
    var EnterpriseURL;
    var connectiontype=-1;
    var token = "";
    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {
            // Add a click event handler for the submit button.
            $('#submit-button').click(submit);
            ajaxindicatorstart("Please wait while we authenticate you..");
            if (window.location.search.indexOf("appmethod") > -1)
            {
                GetConfiguration();
            }
            else if (window.location.search.indexOf("REF") > -1)
            {
                var REF = window.location.search;
                submit(REF,2);
            }
            else (window.location.href.lastIndexOf("#access_token") > -1)
            {
                if (window.location.hash != "")
                {
                    var access_token = window.location.hash.match(/\#(?:access_token)\=([\S\s]*?)\&/)[1];
                    submit(access_token,3);
                }
                
            }
        });
    };
    
    // Submit the name back to the Office add-in.
    function submit(params,int_connection)
    {
        // Get and create the data object.
        if (int_connection ==2)
        {
            Login(params, int_connection);
        }
        else if (int_connection == 3)
        {
            Login('?REF=' + params, int_connection);
        }
        if (params != undefined && params!="") {
            var data = {
                TOKEN: token,
                CondecoURL: EnterpriseURL,
                timestamp: new Date()
            }
        }
        else {
            var name = "";
            var data = {
                name: name,
                timestamp: new Date()
            }
        }
        // Create the JSON and send it to the parent.
        var json = JSON.stringify(data);
        Office.context.ui.messageParent(json);
        //getAccessTokenByPost();
    }
    function GetConfiguration() {
        $.ajax({
            type: 'GET',
            url: location.protocol + "//" + location.host + '/webapi/OfficeApp/SystemInfo?targeturl=' + location.href.split(/[?#]/)[0],
            cache: false,
            async: false,
        }).success(function (data) {
            if (data != null && data)
            {
                var response = JSON.parse(data);
                if (response.success == "True" && parseInt(response.connection)>-1)
                {
                    connectiontype = parseInt(response.connection);
                    EnterpriseURL = response.BaseURL;
                    switch(parseInt(response.connection)) 
                    {
                        case 0:
                            
                            break;
                        case 1:
                            
                            break;
                        case 2:
                        case 3:
                            window.location = response.redirecturi;
                            break;
                        default:
                            break;
                    }
                    
                }
                
            }
        }).fail(function (status) {

        }).always(function () {

        });
        $('.disable-while-sending').prop('disabled', false);
    }
    
    function Login(refid, int_connection) {
        refid = refid + "&connection=" + int_connection;
        $.ajax({
            url: location.protocol + "//" + location.host +'/webapi/OfficeApp/SessionToken' + refid.replace("REF", "ssoRef"),
            type: 'GET',
            cache: false,
            async: false,
        }).success(function (data) {
            if (data != null && data) {
                token = data;
                //sessionStorage.setItem('CondecoToken', token);
            }
        }).fail(function (status) {

        }).always(function () {

        });
    }
    function ajaxindicatorstart(text) {
        if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
            jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="images/pleasewait.gif"><div>' + text + '</div></div><div class="bg"></div></div>');
        }

        jQuery('#resultLoading').css({
            'width': '100%',
            'height': '100%',
            'position': 'fixed',
            'z-index': '10000000',
            'top': '0',
            'left': '0',
            'right': '0',
            'bottom': '0',
            'margin': 'auto'
        });

        jQuery('#resultLoading .bg').css({
            'background': '#ffffff',
            'opacity': '0.4',
            'width': '100%',
            'height': '100%',
            'position': 'absolute',
            'top': '0'
        });

        jQuery('#resultLoading>div:first').css({
            'width': '250px',
            'height': '75px',
            'text-align': 'center',
            'position': 'fixed',
            'top': '2',
            'left': '0',
            'right': '0',
            'bottom': '1',
            'margin': 'auto',
            'font-size': '12px',
            'z-index': '10',
            'color': '#1A47FB',
            'font-family': 'Helvetica'

        });

        jQuery('#resultLoading .bg').height('100%');
        jQuery('#resultLoading').fadeIn(500);
        jQuery('body').css('cursor', 'wait');
    }
    function ajaxindicatorstop() {
        jQuery('#resultLoading .bg').height('100%');
        jQuery('#resultLoading').fadeOut(300);
        jQuery('body').css('cursor', 'default');
    }
})();