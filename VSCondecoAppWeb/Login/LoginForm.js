﻿(function () {
    "use strict";
    var ssopage;
    var EnterpriseURL;
    var token = "";
    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {
           
            // Add a click event handler for the submit button.
            $('#submit-button').click(submit);
            GetConfiguration();
            var TargetUrl = '&TARGET=https://' + location.host + '/Login/LoginForm.html';
            window.location = ssopage + TargetUrl;
                if (window.location.search.indexOf("REF") > -1) {
                    var REF = window.location.search;
                    //sessionStorage.setItem('CondecoREF', REF);
                    submit(REF);
                    
                }
        });
    };
    // Submit the name back to the Office add-in.
    function submit(params)
    {
        // Get and create the data object.
        GetSSOLogin(params);
        if (params != undefined) {
            var data = {
                TOKEN: token,
                CondecoURL: EnterpriseURL,
                timestamp: new Date()
            }
        }
        else {
            var name = $('#name').val();
            var data = {
                name: name,
                timestamp: new Date()
            }
        }
        // Create the JSON and send it to the parent.
        var json = JSON.stringify(data);
        Office.context.ui.messageParent(json);
    }
    function GetConfiguration() {
        $.ajax({
            type: 'GET',
            url: 'https://enterprisev34/webapi/OfficeApp/SystemInfo',
            cache: false,
            async: false,
        }).success(function (data) {
            if (data != null && data) {
                ssopage = data.SSOURL;
                EnterpriseURL = data.BaseURL;
            }
        }).fail(function (status) {

        }).always(function () {

        });
        $('.disable-while-sending').prop('disabled', false);
    }
    function GetSSOLogin(refid) {
        $.ajax({
            url: EnterpriseURL+'/webapi/OfficeApp/SessionToken' + refid.replace("REF", "ssoRef"),
            type: 'GET',
            cache: false,
            async: false,
        }).success(function (data) {
            if (data != null && data) {
                token = data;
                //sessionStorage.setItem('CondecoToken', token);
            }
        }).fail(function (status) {

        }).always(function () {

        });

    }
    
   
})();