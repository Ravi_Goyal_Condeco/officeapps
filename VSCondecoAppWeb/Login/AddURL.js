﻿(function () {
    "use strict";
    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {
        // Add a click event handler for the submit button.
            $(".submit-button").click(function () {
                var value = $("#InputURL").attr('value');
                submit(value);
            });
        });
    };
    function submit(params)
    {
        
        Office.context.ui.messageParent(params);
    }
})();