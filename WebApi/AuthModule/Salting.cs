﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
namespace WebApi.AuthModule
{
    public class Salting
    {
        #region Constants
        const string SALT = "E1F53135E559C253";
        #endregion

        /// <summary>
        /// Generates a random salt and stores using the identifier
        /// </summary>
        /// <returns></returns>
        public static string GetAESSalt()
        {
            return SALT;
        }
    }
    public static class Encryption
    {
        const string AESKEY = "*demo key for aesCryptoprovider*";

        /// <summary>
        /// Hashes a plain text using salt.
        /// </summary>
        /// <param name="plainText">Plain text to be encrypted</param>
        /// <param name="salt">Salt to be used for hashing</param>
        /// <returns></returns>
        public static string HashText(string plainText, string salt)
        {
            SHA256CryptoServiceProvider shaProvider = new SHA256CryptoServiceProvider();
            byte[] encryptedText = null;

            var saltBytes = Guid.Parse(salt).ToByteArray();
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            var saltedTextBytes = plainTextBytes.Concat(saltBytes).ToArray();

            encryptedText = shaProvider.ComputeHash(saltedTextBytes);

            var data = encryptedText.Concat(saltBytes).ToArray();

            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// Encrypts a plain text to Base64String, using a salt
        /// </summary>
        /// <param name="plaintText">Plain text to be encrypted</param>
        /// <param name="salt">Salt to be used for encryption</param>
        /// <returns></returns>s
        public static string EncryptText(string plainText, string salt, string AES = null)
        {
            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            if (AES == null)
                aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(AESKEY);
            else
                aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(AES);
            aesProvider.IV = System.Text.Encoding.ASCII.GetBytes(salt);

            byte[] encrypted;

            using (var memoryStream = new System.IO.MemoryStream())
            {
                using (var streamWriter = new System.IO.StreamWriter(new CryptoStream(memoryStream, aesProvider.CreateEncryptor(), CryptoStreamMode.Write)))
                {
                    streamWriter.Write(plainText);
                }
                encrypted = memoryStream.ToArray();
            }

            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Decrypts a Base64String to plain text, using the salt
        /// </summary>
        /// <param name="plaintText">Plain text to be encrypted</param>
        /// <param name="salt">Salt to be used for encryption</param>
        /// <returns></returns>
        public static string DecryptText(string encryptedText, string salt, string AES = null)
        {
            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            if (AES == null)
                aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(AESKEY);
            else
                aesProvider.Key = System.Text.Encoding.ASCII.GetBytes(AES);
            aesProvider.IV = System.Text.Encoding.ASCII.GetBytes(salt);

            string plainText = string.Empty;

            using (var memoryStream = new System.IO.MemoryStream(Convert.FromBase64String(encryptedText)))
            {
                using (var streamReader = new System.IO.StreamReader(new CryptoStream(memoryStream, aesProvider.CreateDecryptor(), CryptoStreamMode.Read)))
                {
                    plainText = streamReader.ReadToEnd();
                }

            }

            return plainText;
        }
    }
}