﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            //config.EnableCors();
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(name: "ControllersApi", routeTemplate: "api/{controller}/{action}");
            config.Routes.MapHttpRoute("loginresponse", "api/{controller}/{action}/{id}", new { controller = "loginresponse", action = "GetLoginResponse", id = "" }
      );
        }
    }
}
