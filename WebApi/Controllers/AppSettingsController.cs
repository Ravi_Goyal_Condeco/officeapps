﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;
namespace WebApi.Controllers
{
    public class AppSettingsController : ApiController
    {
        IList<AppSettings> appconfig = new List<AppSettings>()
        {

        new AppSettings()
                {
            AppSSOPage=UtilityManager.AppSSOPage,
            EnterpriseURL=UtilityManager.EnterpriseURL,
                },
        };
        [HttpGet]
        [ActionName("getappsettings")]
        public IEnumerable<AppSettings> Get()
        {
            return appconfig.ToList();
        }
    }
}
