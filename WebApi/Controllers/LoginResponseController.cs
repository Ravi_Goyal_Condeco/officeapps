﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WebApi.Models;
using System.Web.Http.Cors;
using Newtonsoft.Json;
namespace WebApi.Controllers
{
    public class LoginResponseController : ApiController
    {
        [HttpGet]
        [ActionName("GetLoginResponse")]
        [Route("api/loginresponse/GetLoginResponse/")]

        public LoginResponse GetLoginResponse(string REF)
        {

            // string authValue = UtilityManager.SendDatatoEnterprise(UtilityManager.HostURL + "/login/sso/startsaml.aspx" + "?REF=" + refid);
            LoginResponse loginresponsemodel = new LoginResponse();
            var authValue = UtilityManager.createsamlrequest(REF);
            if (authValue != null)
            {
                
                loginresponsemodel.LoginResult = authValue.LoginResult;
                loginresponsemodel.MemorableWord = !string.IsNullOrEmpty(authValue.MemorableWord) ? authValue.MemorableWord : "";
                loginresponsemodel.Token = authValue.Token;
                loginresponsemodel.ssoTokenExp = authValue.ssoTokenExp;
                loginresponsemodel.Department = !string.IsNullOrEmpty(authValue.UserInformation.Department) ? authValue.UserInformation.Department : "";
                loginresponsemodel.DomainID = authValue.UserInformation.DomainID;
                loginresponsemodel.Email = !string.IsNullOrEmpty(authValue.UserInformation.Email) ? authValue.UserInformation.Email : "";
                loginresponsemodel.FirstName = !string.IsNullOrEmpty(authValue.UserInformation.FirstName) ? authValue.UserInformation.FirstName : "";
                loginresponsemodel.LastName = !string.IsNullOrEmpty(authValue.UserInformation.LastName) ? authValue.UserInformation.LastName : "";
                loginresponsemodel.Mobile = !string.IsNullOrEmpty(authValue.UserInformation.Mobile) ? authValue.UserInformation.Mobile : "";
                loginresponsemodel.TelephoneNumber = !string.IsNullOrEmpty(authValue.UserInformation.TelephoneNumber) ? authValue.UserInformation.TelephoneNumber : "";
                loginresponsemodel.UserName = !string.IsNullOrEmpty(authValue.UserInformation.UserName) ? authValue.UserInformation.UserName : "";
                //encrypt email with shaEncrypt
                loginresponsemodel.keyA = UtilityManager.SHAEncrypt(loginresponsemodel.Email);
                //encrypt email with JwtEncrypt
                loginresponsemodel.keyJ = UtilityManager.Jwtencode(loginresponsemodel.Email);
                //encrypt token with shaEncrypt
                loginresponsemodel.KeyT = UtilityManager.SHAEncrypt(authValue.Token);
            }
            return loginresponsemodel;

        }
       

    }
}