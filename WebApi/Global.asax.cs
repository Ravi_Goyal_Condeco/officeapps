﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Configuration;
using WebApi.AuthModule;
namespace WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GetApplicationConfiguration();
            GlobalConfiguration.Configuration.MessageHandlers.Add(new ApplicationAuthenticationHandler());
        }
        protected void Application_BeginRequest()
        {
        }
       internal void GetApplicationConfiguration()
        {
            UtilityManager.GetCondecoHostName();
            UtilityManager.EnterpriseURL = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnterpriseURL"])? ConfigurationManager.AppSettings["EnterpriseURL"].ToString():"";
            UtilityManager.PingUserName = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["PingUserName"]) ? ConfigurationManager.AppSettings["PingUserName"].ToString() : "";
            UtilityManager.Pingpassword = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Pingpassword"]) ? ConfigurationManager.AppSettings["Pingpassword"].ToString() : "";
            UtilityManager.PinginstanceId = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["PinginstanceId"]) ? ConfigurationManager.AppSettings["PinginstanceId"].ToString() : "";
            UtilityManager.AppSSOPage = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["AppSSOPage"]) ? ConfigurationManager.AppSettings["AppSSOPage"].ToString() : "";
            UtilityManager.DEBUGSAML = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["DEBUGSAML"]) ? Convert.ToInt32(ConfigurationManager.AppSettings["DEBUGSAML"].ToString()) : 0;
            UtilityManager.Issuer = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Issuer"]) ? ConfigurationManager.AppSettings["Issuer"].ToString() : "";
            UtilityManager.BaseURL = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["BaseURL"]) ? ConfigurationManager.AppSettings["BaseURL"].ToString() : "";
            UtilityManager.LoginApiUrl = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["LoginApiUrl"]) ? ConfigurationManager.AppSettings["LoginApiUrl"].ToString() : "";
            UtilityManager.JwtKey= !string.IsNullOrEmpty(ConfigurationManager.AppSettings["jwt:SymmetricKey"]) ? ConfigurationManager.AppSettings["jwt:SymmetricKey"].ToString() : "";
            UtilityManager.jwtAllowedAudience= !string.IsNullOrEmpty(ConfigurationManager.AppSettings["jwt:AllowedAudience"]) ? ConfigurationManager.AppSettings["jwt:AllowedAudience"].ToString() : "";
            UtilityManager.jwtAllowedIssuer = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["jwt:AllowedIssuer"]) ? ConfigurationManager.AppSettings["jwt:AllowedIssuer"].ToString() : "";
        }
    }
}
