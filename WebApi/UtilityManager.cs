﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Net.Security;
using System.Text;
using WebApi.Appdata;
using Newtonsoft.Json;
using System.Security.Cryptography;
using WebApi.AuthModule;
namespace WebApi
{
    public sealed class UtilityManager
    {
        private UtilityManager()
        {

        }
        private static string _hostName;
        private static string _hostScheme;
        private static string _hostURL;
        private static CookieContainer myContainer = new CookieContainer();
        public static string EnterpriseURL { get; set; }
        public static string PingUserName { get; set; }
        public static string Pingpassword { get; set; }
        public static string PinginstanceId { get; set; }
        public static string AppSSOPage { get; set; }
        public static int DEBUGSAML { get; set; }
        public static string Issuer { get; set; }
        public static string BaseURL { get; set; }
        public static string LoginApiUrl { get; set; }
        public static string JwtKey { get; set; }
        public static string jwtAllowedAudience { get; set; }
        public static string jwtAllowedIssuer { get; set; }
        public static string AuthKey { get; set; }


        public static string HostName
        {
            get
            {
                /* if (string.IsNullOrEmpty(hostName))
                 {
                     hostName = ConfigurationManager.AppSettings["CondecoHost"].ToString();

                 }*/
                return _hostName;
            }


            set
            {
                _hostName = value;
            }
        }
        public static string HostScheme
        {
            get
            {
                if (string.IsNullOrEmpty(_hostScheme))
                {
                    _hostScheme = Uri.UriSchemeHttp;

                }
                return _hostScheme;
            }


            set { _hostScheme = value; }
        }
        public static string GetCondecoHostName()
        {
            string hostvalue = "";
            try
            {
                string currentScheme = Uri.UriSchemeHttp;
                hostvalue = ConfigurationManager.AppSettings["EnterpriseUrl"].ToString();
                if (hostvalue.Contains("http://") || hostvalue.Contains("https://"))
                {
                    Uri currentUri = new Uri(hostvalue);
                    if (currentUri.Port == 80 || currentUri.Port == 0 || currentUri.Port == 443)
                        hostvalue = currentUri.Host;
                    else if (currentUri.Port > 0)
                        hostvalue = currentUri.Host + ":" + currentUri.Port;
                    currentScheme = currentUri.Scheme + Uri.SchemeDelimiter;
                }
                HostName = hostvalue;
                HostScheme = currentScheme;
            }
            catch { }
            return hostvalue;
        }
        public static string SendDatatoEnterprise(string url, string data = null)
        {
            string responsereceived = "";
            try
            {
              //  responsereceived = SendDataToSever(url, data);
            }
            catch { }
            return responsereceived;
        }
        public static bool IsHostContactable(string host)
        {
            bool isContactable = false;
            try
            {
                string url = GetPath(host, "/login/logcheck.aspx");
                string data = "1";
                string res = SendDatatoEnterprise(url, data);
                isContactable = res.Contains("1") ? true : false;
            }
            catch
            {
            }
            return isContactable;
        }
        private static string GetPath(string host, string path)
        {
            string currentHostName = "";
            try
            {
                currentHostName = host;

                if (currentHostName.Contains("http://") || currentHostName.Contains("https://"))
                {
                    currentHostName = currentHostName + path;
                }
                else
                {
                    currentHostName = HostURL + path;
                }
            }
            catch
            {
                currentHostName = "";
            }
            return currentHostName;

        }
        public static string HostURL
        {
            get
            {
                string result = string.Empty;
                string currentHostName = HostName;
                if (!string.IsNullOrEmpty(currentHostName))
                {
                    if (HostScheme.Contains(Uri.SchemeDelimiter))
                        result = HostScheme + currentHostName;
                    else
                        result = HostScheme + Uri.SchemeDelimiter + currentHostName;
                }

                return result;
            }
            set { _hostURL = value; }
        }
       
        public static LoginResponse createsamlrequest(string ssoref)
        {
            LoginResponse response = default(LoginResponse);
            var param = new System.Collections.Specialized.NameValueCollection();
            param.Add("REF", ssoref);
            using (WebClient client = new WebClient())
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                client.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                client.Headers.Add(HttpRequestHeader.Cookie, "excuse the Microsoft bug");
                client.Headers.Add("ping.uname", UtilityManager.PingUserName);
                client.Headers.Add("ping.pwd", UtilityManager.Pingpassword);
                client.Headers.Add("ping.instanceId", UtilityManager.PinginstanceId);
                var result = client.UploadValues(UtilityManager.BaseURL, "POST", param);
                var resultString = Encoding.ASCII.GetString(result);
                var json = Newtonsoft.Json.JsonConvert.DeserializeObject<SsoResponse>(resultString);
                if(json.subject!=null)
                {
                    response = AuthenticateUserName(json.subject, "sso", callingFrom: 2);
                    var userInfo = new LoginResponse.ADUserInfo();
                    userInfo.Email = json.mail;
                    userInfo.UserName = json.subject;
                    userInfo.FirstName = json.first;
                    userInfo.LastName = json.sn;
                    userInfo.DomainID = -1;
                    response.UserInformation = userInfo;

                }
                else
                {
                    HttpContext.Current.Response.Write("Identity not found. Please close this window and try again later.");
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
                return response;
        }
        public static LoginResponse AuthenticateUserName(string username, string connectionType, int callingFrom, int domainID = -1, string domainFriedlyName = "")
        {
            DateTime dependecyStart = DateTime.UtcNow;
           // bool dependecySuccess = true;

            LoginResponse authenticationResponse = default(LoginResponse);
            object loginRequest = null;
            try
            {
                loginRequest = new
                {
                    UserName = username,
                    ConnectionType = 3,
                    DomainServers = "",
                    CallingFrom = 2
                };
                var apiResponse = MakeWebRequest<LoginResponse>("/auth/authenticateusername", loginRequest, true);
                if (apiResponse.ResponseCode == WebResponseCode.OK)
                {
                    authenticationResponse = apiResponse.Result;
                }
            }
            catch (WebException wex)
            {

                throw wex;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
            return authenticationResponse;
        }
        public static ApiResponse<T> MakeWebRequest<T>(string endpointName, object data, bool useJWT = false)
        {
            endpointName = LoginApiUrl + endpointName;
            WebClient client = new WebClient();
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            SetWebRequestHeaders(client, useJWT);
            var jsonRequest = JsonConvert.SerializeObject(data);
            var byteDate = Encoding.UTF8.GetBytes(jsonRequest);
            var result = client.UploadData(endpointName, "POST", byteDate);
            return JsonConvert.DeserializeObject<ApiResponse<T>>(System.Text.Encoding.UTF8.GetString(result));
        }
        private static void SetWebRequestHeaders(WebClient client, bool useJWT = false)
        {
            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            client.Headers.Add(HttpRequestHeader.Accept, "application/json");

            if (useJWT)
            {
                var payload = new
                {
                    iss = UtilityManager.jwtAllowedIssuer,
                    exp = (Int32)(DateTime.UtcNow.AddMinutes(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds,
                    aud = UtilityManager.jwtAllowedAudience
                };
                client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + JsonWebToken.Encode(payload, UtilityManager.JwtKey, JwtHashAlgorithm.HS256));
            }
        }
        public static string SHADecrypt(string stringtoDecrypt)
        {
            return Encryption.DecryptText(stringtoDecrypt, Salting.GetAESSalt());
        }
        public static string SHAEncrypt(string stringtoEncrypt)
        {
            return Encryption.EncryptText(stringtoEncrypt, Salting.GetAESSalt());
        }
        public static string Jwtencode(string stringtoencrypt)
        {
            var payload = new
            {
                iss = UtilityManager.jwtAllowedIssuer,
                exp = (Int32)(DateTime.UtcNow.AddMinutes(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds,
                aud = UtilityManager.jwtAllowedAudience
            };
            return stringtoencrypt = JsonWebToken.Encode(payload, UtilityManager.JwtKey, JwtHashAlgorithm.HS256);
        }

    }
}