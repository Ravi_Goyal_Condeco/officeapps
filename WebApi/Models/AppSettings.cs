﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class AppSettings
    {
        public string AppSSOPage { get; set; }
        public string EnterpriseURL { get; set; }
    }
}