﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Appdata
{
    public class LoginResponse
    {
        [Serializable]
        public class ADUserInfo
        {
            public string UserName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string TelephoneNumber { get; set; }
            public string Mobile { get; set; }
            public string Department { get; set; }
            public int DomainID { get; set; }
        }

        public int LoginResult { get; set; }
        public string Token { get; set; }
        public string ssoTokenExp { get; set; } = "";
        public string MemorableWord { get; set; }
        public ADUserInfo UserInformation { get; set; }
    }
}