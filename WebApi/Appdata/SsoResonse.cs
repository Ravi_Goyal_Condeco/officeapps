﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Appdata
{
    public class SsoResponse
    {
        public string authnCtx { get; set; }
        public string mail { get; set; }
        public string partnerEntityID { get; set; }
        public string sn { get; set; }
        public string subject { get; set; }
        public string instanceId { get; set; }
        public string sessionid { get; set; }
        public string authnInst { get; set; }
        public string first { get; set; }
    }
    public class ApiResponse
    {
        public WebResponseCode ResponseCode { get; set; }
        public object ResponseData { get; set; }
    }
    public class ApiResponse<T>
    {
        public WebResponseCode ResponseCode { get; set; }
        public object ResponseData { get; set; }

        public T Result { get; set; }
    }
    public enum WebResponseCode
    {
        OK = 1,

        #region Informative (101 - 199)
        #endregion

        #region Client Action (201 - 299)
        /// <summary>
        /// Makes the client redirect to the Change Password webpage
        /// </summary>
        ChangePassword = 201,

        /// <summary>
        /// Makes the client redirect to a URL contained in the associated data
        /// </summary>
        RedirectToUrl = 202,
        #endregion

        #region Error Handling(301 - 399)
        UnhandledErrorOccured = 301
        #endregion
    }
}